%title: COUCHBASE
%author: xavki


# 04 COUCHBASE : Installation


<br>

Spécifications : 

		* 4GB ram mini (reco 16) > 1G possible
		* 2Ghz (3Ghz / 6 coeurs si XCDR)
    * 8G SSD (pas de CIFS/NFS)
		* Linux / Microsoft / MacOS

Cf : cluster à moins de 3 noeuds 
https://docs.couchbase.com/server/7.0/install/deployment-considerations-lt-3nodes.html

-----------------------------------------------------------------

# 04 COUCHBASE : Installation

<br>

Autres :
		
		* NTP

		* Disable THP (Transparent Huge Pages > affectation de mémoire contigue)

		* vm.swappiness > 1
<br>

Liste des ports :
https://docs.couchbase.com/server/current/install/install-ports.html

-----------------------------------------------------------------

# 04 COUCHBASE : Installation

INSTALLATION

<br>

* installation du dépôt 

```
curl -O https://packages.couchbase.com/releases/
couchbase-release/couchbase-release-1.0-amd64.deb
sudo dpkg -i couchbase-release-1.0-amd64.deb
sudo apt update
```

-----------------------------------------------------------------

# 04 COUCHBASE : Installation

<br>

* pour l'enterprise edition

```
#enterprise edition
sudo apt-get install couchbase-server
```

* pour la community edition

```
sudo apt-get install couchbase-server-community
```

-----------------------------------------------------------------

# 04 COUCHBASE : Installation

<br>

* pour la version bêta

```
wget -q https://packages.couchbase.com/releases/7.0.0-beta/
couchbase-server-community_7.0.0-beta-ubuntu20.04_amd64.deb
sudo dpkg -i couchbase-server-community_7.0.0-beta-ubuntu20.04_amd64.deb
```

