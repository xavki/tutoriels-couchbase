%title: COUCHBASE
%author: xavki


# 11 COUCHBASE : Users et Groups

<br>

GUI

Attention : fonctionnalités principalement EE

Security > Users & Groups
	
LDAP

Users = 1 à plusieurs groups

Possiblité de mapper les groupes LDAP sur les groupes CB

------------------------------------------------------------------------------------------------------

# 11 COUCHBASE : Users & Groups


<br>

* CLI

```
/opt/couchbase/bin/couchbase-cli user-manage
-c http://couch1:8091 --username xavki --password 123456
--set --rbac-username user1 --rbac-password 123456 --rbac-name "user1"
--roles data_reader[xavki],data_writer[xavki],fts_searcher[xavki],
fts_admin[xavki],query_delete[xavki],query_insert[xavki],
query_select[xavki],query_update[xavki],query_manage_index[xavki]
--auth-domain local
```

Attention : auth-domain > local/external
