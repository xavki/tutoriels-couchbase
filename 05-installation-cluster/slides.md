%title: COUCHBASE
%author: xavki


# 05 COUCHBASE : Installation Cluster


INSTALLATION

<br>

* installation du dépôt 

```
curl -O https://packages.couchbase.com/releases/
couchbase-release/couchbase-release-1.0-amd64.deb
sudo dpkg -i couchbase-release-1.0-amd64.deb
sudo apt update
```

-----------------------------------------------------------------

# 05 COUCHBASE : Installation

<br>

* pour l'enterprise edition

```
#enterprise edition
sudo apt-get install couchbase-server
```

* pour la community edition

```
sudo apt-get install couchbase-server-community
```

-----------------------------------------------------------------

# 05 COUCHBASE : Installation

<br>

* pour la version bêta

```
wget -q https://packages.couchbase.com/releases/7.0.0-beta/
couchbase-server-community_7.0.0-beta-ubuntu20.04_amd64.deb
sudo dpkg -i couchbase-server-community_7.0.0-beta-ubuntu20.04_amd64.deb
```

