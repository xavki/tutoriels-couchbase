%title: COUCHBASE
%author: xavki


# 07 COUCHBASE : CLI - installation d'un cluster

<br>

INSTALLATION

<br>

* installation du dépôt 

```
curl -O https://packages.couchbase.com/releases/
couchbase-release/couchbase-release-1.0-amd64.deb
sudo dpkg -i couchbase-release-1.0-amd64.deb
sudo apt update
```

-----------------------------------------------------------------

# 07 COUCHBASE : CLI - installation d'un cluster

<br>

* pour l'enterprise edition

```
#enterprise edition
sudo apt-get install couchbase-server
```

* pour la community edition

```
sudo apt-get install couchbase-server-community
```

-----------------------------------------------------------------

# 07 COUCHBASE : CLI - installation d'un cluster

<br>

* pour la version bêta

```
wget -q https://packages.couchbase.com/releases/7.0.0-beta/couchbase-server-enterprise_7.0.0-beta-ubuntu20.04_amd64.deb
sudo dpkg -i couchbase-server-enterprise_7.0.0-beta-ubuntu20.04_amd64.deb
```

-----------------------------------------------------------------

# 07 COUCHBASE : CLI - installation d'un cluster


# Disable THP
sudo /bin/bash -c 'echo never > /sys/kernel/mm/transparent_hugepage/enabled'
sudo sed -i s/"quiet splash"/"transparent_hugepage=never quiet splash"/g /etc/default/grub
sudo update-grub

# Set swappiness to 0
sudo /bin/bash -c 'echo vm.swappiness = 0 > /etc/sysctl.d/99-sysctl.conf'
sudo sysctl -p


<br>

* initialisation du cluster

```
/opt/couchbase/bin/couchbase-cli cluster-init -c couch1 --cluster-username xavki --cluster-password 123456 --services data,query,index,fts,eventing,analytics,backup --cluster-ramsize 2048 --cluster-analytics-ramsize 128 --cluster-eventing-ramsize 128 --cluster-fts-ramsize 256 --cluster-index-ramsize 256
/opt/couchbase/bin/couchbase-cli/couchbase-cli server-list -c couch1
```

Notes: 

	* export PATH="/opt/couchbase/bin/:$PATH"

-----------------------------------------------------------------

# 07 COUCHBASE : CLI - installation d'un cluster

<br>

* join cluster

```
/opt/couchbase/bin/couchbase-cli server-add -c http://couch1:8091 --username xavki --password 123456 --server-add 192.168.16.11 --server-add-username xavki --server-add-password 123456 --services data,query,index,fts,eventing,analytics,backup
```

<br>

* rebalance

```
couchbase-cli rebalance -c http://couch1:8091 --username xavki --password 123456
```
