%title: COUCHBASE
%author: xavki


# 08 COUCHBASE : Types de Buckets

<br>

* Bucket ~ Database > composés de vbucket (shards/partitions)

<br>

3 types de Buckets :

		* Couchbase

		* Ephemeral

		* Memcached (Deprecated)

----------------------------------------------------------------------

# 08 COUCHBASE : Types de Buckets

<br>

COUCHBASE

		* Assure la persistence sur disque

		* Réplication via le DCP (Database Change Protocol)

		* Full RAM > éviction (et écriture sur disque si pas déjà faite)
				* queue d'écriture disque (iops importants)

		* Eviction : full ou partiel (data seulement)

		* Taille de document 20MB

		* Statistiques disques disponibles

		* TTL : niveau bucket ou documents

		* Durabilité : quorum d'écriture/lecture

----------------------------------------------------------------------

# 08 COUCHBASE : Types de Buckets

<br>

EPHEMERAL

		* Uniquement en RAM

		* Réplication possible (diff avec Memcached)

		* Règle d'éjection :
				* pas d'éjection = blocage des enregistrements
				* éjection de la data

		* TTL : niveau bucket ou documents

		* Purge des tombstones 
				* purge des données de la RAM
				* metadatas supprimées à fréquence régulière

		* Durabilité : quorum d'écriture/lecture
