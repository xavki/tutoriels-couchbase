%title: COUCHBASE
%author: xavki


# 01 COUCHBASE : INTRODUCTION


<br>

Site : https://www.couchbase.com

2010/2011 : fusion de Membase et CouchOne
		* technologies memcached + couchdb

<br>

* moteur de base de données NOSQL

* freemium : community edition / enterprise edition

<br>

* système distribué / cluster

<br>

* clef/valeur orientée documents (multi-modèles)

-------------------------------------------------------------------------------------

# COUCHBASE : INTRODUCTION

<br>

* Quelques chiffres et noms :
		* linkedin : 10 millions requêtes par seconde (300 clusters)
		* eBay : 1.3 milliards produits (4000 serveurs)
		* paypal : 1 milliards de documents
		* carrefour : catalogue de produits
		* LVMH, coyote, revolut, GE, pfizer...

https://engineering.linkedin.com/blog/2018/05/evolution-of-couchbase-at-linkedin


-------------------------------------------------------------------------------------

# COUCHBASE : INTRODUCTION

<br>

* memory first : performance d'accès à la donnée (lecture/écriture)
		* copie asynchrone des données sur disque
		* RAM = élément crucial
		* gestion du swap (swappiness = 0/1)

<br>

* documents sous forme de données semi-structurées
		* json
		* (binaire)

<br>

* basé sur memcached + persistence + replication


-------------------------------------------------------------------------------------

# COUCHBASE : INTRODUCTION

<br>

ACID : à l'échelle des documents

	* Atomicité : mise à jour du groupe d'opérations sinon rien (transactions)

	* Consistency : maintien de la consistence des réplicas, indexes et XDCR

	* Isolation : gestion des lectures/écritures en concurence 
				* locks : optimitic ou pesimistic

	* Durability : résistance à la panne 


Note : https://blog.couchbase.com/acid-properties-couchbase-part-1/

-------------------------------------------------------------------------------------

# COUCHBASE : INTRODUCTION


<br>

* fault tolerance :
		* replication factor
		* rack awareness = grouping
		* xcdr

<br>

* GUI :
		* configuration
		* statistiques
		* analyses
		* requêtes
		* diagnostiques
		* maintenance courante

<br>

* interrogation via API (curl, librairies...)
		* réponse en format JSON

-------------------------------------------------------------------------------------

# COUCHBASE : INTRODUCTION


<br>

* N1QL : language centré sur le sql

<br>

* Index

<br>

* XCDR : réplication cross datacenter

<br>

*  différents rôles (par noeuds)

<br>

* gestion utilisateurs/droits

* audit/analyse
