%title: COUCHBASE
%author: xavki


# 03 COUCHBASE : Concepts


<br>

MEMORY FIRST

		* data d'abord écrite en RAM

		* puis écrite sur disk (disk queue)

		* et répliquée (replication queue)

----------------------------------------------------------------------------

# 03 COUCHBASE : Concepts

<br>

REBALANCE

		* opération de redistribution des vBuckets (partitions)

		* standard ou swap rebalance (entrée/sortie)


----------------------------------------------------------------------------

# 03 COUCHBASE : Concepts

<br>

FAILOVER

		* sortie de noeuds

		* manuel ou automatique

----------------------------------------------------------------------------

# 03 COUCHBASE : Concepts


<br>

SERVICES

		* service
					* fonctions du moteur + ressources
					* peut être spécifique par node

		* data : de base pour le sotckage des documents
				* mode key/value > connaître les clefs

		* query : permet d'utiliser le N1QL (nickel)

		* index : création d'index (nécessaire pour query et analytics)

		* full text search (FTS) : création d'indexs plain text
				* spécificités aux langages...
				* différent de Lucene

		* analytics : pour faciliter le travail sur les opérations longues
				* group, sort... (memory et cpu)

----------------------------------------------------------------------------

# 03 COUCHBASE : Concepts


<br>

EVICTION

		* quels documents garder en RAM ?

		* MRU (most recently used)

		* low/high water mark : 75%/85%
				* premier temps les réplicas sont sortis
				* second temps les vbuckets

		* 2 types d'éviction : full / value

