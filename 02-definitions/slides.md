%title: COUCHBASE
%author: xavki


# 02 COUCHBASE : Définitions & Concepts


DEFINITIONS

<br>

CLUSTER

		* composé de 1 à plusieurs noeuds

		* contient de 1 à plusieurs Buckets (éviter de surcharger)

		* allocation de mémoire utilisable par noeud par CB

----------------------------------------------------------------------------------------------

# 02 COUCHBASE : Définitions & Concepts

<br>

BUCKET

		* allocation de mémoire spécifique par bucket

		* élément logique contenant les indexs/documents
				* regroupement logique d'éléments physiques

		* assimilable à une Database (sql)  / Keyspace (cassandra)

		* différents types : couchbase / ephemeral / memcached (deprecated)


----------------------------------------------------------------------------------------------

# 02 COUCHBASE : Définitions & Concepts


<br>

vBUCKETS

		* élément de distribution

		* élément de réplication

		* 1024 vBuckets maximum par bucket

		* assimilable au sharding (ES)

		* map de distribution de vBuckets et réplicas

----------------------------------------------------------------------------------------------

# 02 COUCHBASE : Définitions & Concepts

<br>

DOCUMENTS

		* clef/valeur

		* clef = id document

		* valeur = json / binaire

		* composé de "fields"

		* metadata

		* appartient à 1 vBucket (éventuellement répliqué)

----------------------------------------------------------------------------------------------

# 02 COUCHBASE : Définitions & Concepts

<br>

REPLICATION

		* duplication des partitions (en rapport avec le nb de noeuds)

		* objectif de haut-dispo

		* utilisé en cas de perte du vBucket principal

----------------------------------------------------------------------------------------------

# 02 COUCHBASE : Définitions & Concepts

<br>

CLIENT

		* smart-client ou non

		* formule la requête et l'envoi au bon noeud

		* smart-client : dispose de la map et la maintient à jour

		* non-smart : nécessité d'un proxy (moxy)
